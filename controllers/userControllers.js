const User = require ("./../models/User");

module.exports.register = (reqBody) => {
	let newUser = new User ({
		firstName: reqBody.firstName,
		lastName: reqBody.lastName,
		email: reqBody.email,
		mobileNo: reqBody.mobileNo,
		password: reqBody.password
	})

	return newUser.save().then((result, error)=>{
		if (error){
			return error
		} else {
			return result
		}
	})
}