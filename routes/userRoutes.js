const express = require ("express");
const router = express.Router();

const userController = require("./../controllers/userControllers");

router.post("/register", (req, res)=>{
	userController.register(req.body).then(result=>res.send(result));
})

module.exports = router;